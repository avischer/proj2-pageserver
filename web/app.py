from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/<argument>")
def send_data(argument):
    if (".." in argument) or ("//" in argument) or ("~" in argument):
        return render_template("403.html"), 403
    try:
        return render_template(argument), 200
    except:
        return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
