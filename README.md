Author: Alex Vischer
Contact: avischer@uoregon.edu

This project functions using docker to create an image with the dockerfile located in /web. This 
image will operate, outputting requested documents at port 5000. One can access this in a broswer 
by searching "localhost:5000/".

Any request after the / will be diplayed in the browser if it exists, and a 404/403 error 
otherwise and depending on the circumstances of your request.
